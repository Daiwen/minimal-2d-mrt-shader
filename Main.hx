class Main extends hxd.App {
	private var objectScene : h2d.Scene;
	public var renderTarget : h3d.mat.Texture;
	public var normalsTarget : h3d.mat.Texture;

	private var lightScene : h2d.Scene;
	var light : h2d.Object;
	var lshader : Shaders.Light;
	public var lightTarget : h3d.mat.Texture;

	private var postScene : h2d.Scene;

	// Boot
	static function main() {
		hxd.Res.initEmbed();
        new Main();
	}

	// Engine ready
	override function init() {
		super.init();

		objectScene = new h2d.Scene();
		new RenderContext(objectScene);

		lightScene = new h2d.Scene();

		renderTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		normalsTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		lightTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);

		postScene = new h2d.Scene();
		var bg = new h2d.Bitmap(h2d.Tile.fromTexture(renderTarget));
		var shader = new Shaders.AddColor(renderTarget, lightTarget);
		bg.addShader(shader);

		postScene.add(bg, 1);

		engine.backgroundColor = 0xa2a2a2;

		createScene(objectScene);
		createLight(lightScene);
	}

	private function createScene(s2d: h2d.Scene) {
		objectScene.scaleX = objectScene.scaleY = 4;
		var t = hxd.Res.my_sprite.toTile();
		var sprite = new h2d.Bitmap(t, s2d);

		sprite.x = (engine.width/2 - t.width/2) / 4;
		sprite.y = (engine.height/2 - t.height/2) / 4;

		var shader = new Shaders.DrawNormals(hxd.Res.my_normals.toTile());
		shader.setPriority(50);
		sprite.addShader(shader);

		/*
		var shader = @:privateAccess {
			var ctx = cast(objectScene.ctx, RenderContext);
			var shdr = ctx.baseShaderList;
			shdr.next = sprite.shaders;
			ctx.manager.compileShaders(ctx.shaderList);
		}
		var toString = hxsl.Printer.shaderToString.bind(_, true);
		var debug = "// vertex:\n" + toString(shader.vertex.data) + "\n\nfragment:\n" + toString(shader.fragment.data);
		js.Browser.console.log(debug);
		@:privateAccess {
			var ctx = objectScene.ctx;
			var shdr = ctx.baseShaderList;
			shdr.next = null;
		}
		*/
	}

	private function createLight(s2d: h2d.Scene) {
		var radius = 300;
		var l = new h2d.Graphics(s2d);
		l.beginFill(0xffffff, 1.);
		l.drawCircle(0, 0, radius);
		l.endFill();

		l.x = engine.width/2;
		l.y = engine.height/2;

		light = l;

		lshader = new Shaders.Light(normalsTarget);
		lshader.width = engine.width;
		lshader.height = engine.height;
		lshader.radius = radius;
		l.addShader(lshader);

		var shader = @:privateAccess {
			var ctx = s2d.ctx;
			var shdr = ctx.baseShaderList;
			shdr.next = l.shaders;
			ctx.manager.compileShaders(ctx.baseShaderList);
		}
		var toString = hxsl.Printer.shaderToString.bind(_, true);
		var debug = "// vertex:\n" + toString(shader.vertex.data) + "\n\nfragment:\n" + toString(shader.fragment.data);
		js.Browser.console.log(debug);
		@:privateAccess {
			var ctx = objectScene.ctx;
			var shdr = ctx.baseShaderList;
			shdr.next = null;
		}
	}

	override function render (e:h3d.Engine) {
		engine.pushTargets([renderTarget, normalsTarget]);

		engine.clear(0, 1);
		objectScene.render(e);

		engine.popTarget();

		engine.pushTarget(lightTarget);

		engine.clear(0, 1);
		lightScene.render(e);

		engine.popTarget();

		postScene.render(e);
	}

	override function update (dt : Float) {
		lshader.lightSource.x = light.x = hxd.Window.getInstance().mouseX;
		lshader.lightSource.y = light.y = hxd.Window.getInstance().mouseY;
		
	}
}
