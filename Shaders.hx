class InitNormals extends hxsl.Shader {

	static var SRC = {
		var pixelNormal : Vec4;

		function __init__() {
			pixelNormal = vec4(0, 0, 1, 0);
		}

	};
}

class SetNormals extends hxsl.Shader {

	static var SRC = {

		var output : {
			var position : Vec4;
			var color : Vec4;
			var normal : Vec4;
		};

		var pixelNormal : Vec4;

		function fragment() {
			output.normal = pixelNormal;
		}
	};
}


class DrawNormals extends hxsl.Shader {
    static var SRC = {
		@param var normals : Sampler2D;

		@var var calculatedUV : Vec2;

		var pixelNormal : Vec4;

		function fragment() {
			pixelNormal = normals.get(calculatedUV);
		}
    }


	public function new(nt : h2d.Tile) {
		super();

		var norm = new h2d.Bitmap(nt);
		normals = norm.tile.getTexture();
		normals.filter = Nearest;
	}
}

class Light extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

        @param var color : Vec4;

		@param var normals : Sampler2D;
		@param var lights : Sampler2D;

		@param var width : Float;
		@param var height : Float;

		@param var lightSource : Vec2;
		@param var radius : Float;

        function fragment() {
			var pos = ((outputPosition.xy + vec2(1)) * 0.5) * vec2(width, height);
			var coord = ((outputPosition.xy + vec2(1)) * 0.5);

			var normal = normals.get(coord);
			var pixelnormal = 2*normal.xy - vec2(1);

			var toLight = lightSource - pos;

			if (normal.w == 0) {
				output.color = vec4(0);
			} else {
				var brightness = clamp(dot(normalize(toLight), pixelnormal), 0.0, 1.0);
				brightness *= 1.3 * clamp(1.0 - (length(toLight) / radius), 0.0, 1.0);

				output.color = color * brightness;
			}
        }
    }


	public function new(nt : h3d.mat.Texture) {
		super();

		color = h3d.Vector.fromColor(0x79acb9);
		color.w = 1;

		normals = nt;
		normals.filter = Nearest;
	}
}

class AddColor extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@param var bgTexture : Sampler2D;
		@param var lightTexture : Sampler2D;

        function fragment() {
			var coord = calculatedUV;

			var light = lightTexture.get(coord);
			var bg = bgTexture.get(coord);

			output.color = light + bg;
        }
    }


	public function new(t : h3d.mat.Texture, lt : h3d.mat.Texture) {
		super();

		bgTexture = t;
		bgTexture.filter = Nearest;

		lightTexture = lt;
		lightTexture.filter = Nearest;
	}
}
