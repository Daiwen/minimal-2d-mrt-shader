@:access(h2d.Scene)
class RenderContext extends h2d.RenderContext {
	var shaderList : hxsl.ShaderList;

	public function new(scene) {
		super(scene);

		var initNormalsShader = new Shaders.InitNormals();
		initNormalsShader.setPriority(0);

		var shdr = new Shaders.SetNormals();
		shdr.setPriority(100);

		baseShaderList = new hxsl.ShaderList(shdr);

		shaderList = new hxsl.ShaderList(initNormalsShader, baseShaderList);
		shaderList = new hxsl.ShaderList(baseShader, shaderList);

		manager.setOutput([Value("output.color"), Value("output.normal")]);

		scene.renderer = this;
	}

	override public function begin() {
		texture = null;
		currentObj = null;
		bufPos = 0;
		stride = 0;
		viewA = scene.viewportA;
		viewB = 0;
		viewC = 0;
		viewD = scene.viewportD;
		viewX = scene.viewportX;
		viewY = scene.viewportY;

		targetFlipY = engine.driver.hasFeature(BottomLeftCoords) ? -1 : 1;
		baseFlipY = engine.getCurrentTarget() != null ? targetFlipY : 1;
		inFilter = null;
		manager.globals.set("time", time);
		manager.globals.set("global.time", time);
		// todo : we might prefer to auto-detect this by running a test and capturing its output
		baseShader.pixelAlign = #if flash true #else false #end;
		baseShader.halfPixelInverse.set(0.5 / engine.width, 0.5 / engine.height);
		baseShader.viewportA.set(scene.viewportA, 0, scene.viewportX);
		baseShader.viewportB.set(0, scene.viewportD * -baseFlipY, scene.viewportY * -baseFlipY);
		baseShader.filterMatrixA.set(1, 0, 0);
		baseShader.filterMatrixB.set(0, 1, 0);
		baseShaderList.next = null;
		initShaders(shaderList);
		engine.selectMaterial(pass);
		textures.begin();
	}

	override public function pushTarget( t : h3d.mat.Texture, startX = 0, startY = 0, width = -1, height = -1 ) {
		flush();
		engine.pushTarget(t);
		initShaders(shaderList);

		var entry = targetsStack[targetsStackIndex++];
		if ( entry == null ) {
			entry = { t: null, va: 0, vb: 0, vc: 0, vd: 0, vx: 0, vy: 0, hasRZ: false, rzX: 0, rzY: 0, rzW: 0, rzH: 0 };
			targetsStack.push(entry);
		}
		entry.t = curTarget;
		entry.va = viewA;
		entry.vb = viewB;
		entry.vc = viewC;
		entry.vd = viewD;
		entry.vx = viewX;
		entry.vy = viewY;
		entry.hasRZ = hasRenderZone;
		entry.rzX = renderX;
		entry.rzY = renderY;
		entry.rzW = renderW;
		entry.rzH = renderH;

		if( width < 0 ) width = t == null ? scene.width : t.width;
		if( height < 0 ) height = t == null ? scene.height : t.height;

		viewA = 2 / width;
		viewB = 0;
		viewC = 0;
		viewD = 2 / height;
		viewX = -1 - startX * viewA;
		viewY = -1 - startY * viewD;

		baseShader.halfPixelInverse.set(0.5 / (t == null ? engine.width : t.width), 0.5 / (t == null ? engine.height : t.height));
		baseShader.viewportA.set(viewA, viewC, viewX);
		baseShader.viewportB.set(viewB * -targetFlipY, viewD * -targetFlipY, viewY * -targetFlipY);
		curTarget = t;
		currentBlend = null;
		if( hasRenderZone ) clearRZ();
	}

	override public function popTarget() {
		flush();
		if( targetsStackIndex <= 0 ) throw "Too many popTarget()";
		engine.popTarget();

		var tinf = targetsStack[--targetsStackIndex];
		var t : h3d.mat.Texture = curTarget = tinf.t;
		viewA = tinf.va;
		viewB = tinf.vb;
		viewC = tinf.vc;
		viewD = tinf.vd;
		viewX = tinf.vx;
		viewY = tinf.vy;
		var flipY = t == null ? -baseFlipY : -targetFlipY;

		initShaders(shaderList);
		baseShader.halfPixelInverse.set(0.5 / (t == null ? engine.width : t.width), 0.5 / (t == null ? engine.height : t.height));
		baseShader.viewportA.set(viewA, viewC, viewX);
		baseShader.viewportB.set(viewB * flipY, viewD * flipY, viewY * flipY);

		if ( tinf.hasRZ ) setRZ(tinf.rzX, tinf.rzY, tinf.rzW, tinf.rzH);
	}

	@:access(h2d.Drawable)
	override function beginDraw( obj : h2d.Drawable, texture : h3d.mat.Texture, isRelative : Bool, hasUVPos = false ) {
		if( onBeginDraw != null && !onBeginDraw(obj) )
			return false;

		var stride = 8;
		if( hasBuffering() && currentObj != null && (texture != this.texture || stride != this.stride || obj.blendMode != currentObj.blendMode || obj.filter != currentObj.filter) )
			flush();
		var shaderChanged = false, paramsChanged = false;
		var objShaders = obj.shaders;
		var curShaders = currentShaders.next;
		while( objShaders != null && curShaders != null ) {
			var s = objShaders.s;
			var t = curShaders.s;
			objShaders = objShaders.next;
			curShaders = curShaders.next;
			var prevInst = @:privateAccess t.instance;
			if( s != t )
				paramsChanged = true;
			s.updateConstants(manager.globals);
			if( @:privateAccess s.instance != prevInst )
				shaderChanged = true;
		}
		if( objShaders != null || curShaders != null || baseShader.isRelative != isRelative || baseShader.hasUVPos != hasUVPos || baseShader.killAlpha != killAlpha )
			shaderChanged = true;
		if( shaderChanged ) {
			flush();
			baseShader.hasUVPos = hasUVPos;
			baseShader.isRelative = isRelative;
			baseShader.killAlpha = killAlpha;
			baseShader.updateConstants(manager.globals);
			baseShaderList.next = obj.shaders;
			initShaders(shaderList);
		} else if( paramsChanged ) {
			flush();
			if( currentShaders != shaderList ) throw "!";
			// the next flush will fetch their params
			currentShaders.next = obj.shaders;
		}

		this.texture = texture;
		this.stride = stride;
		this.currentObj = obj;

		return true;
	}
}
